import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarProfileComponent } from './navbar-profile/navbar-profile.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { CommonService } from './service/common/common.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegVerifyComponent } from './reg-verify/reg-verify.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthService } from './service/auth/auth.service';
import { AuthGuard } from './service/auth/auth.guard';
import { TokenInterceptorService } from './service/auth/token-interceptor.service';
import { PartnerProfileComponent } from './profile/components/partner-profile/partner-profile.component';
import { ProfileEditComponent } from './profile/components/profile-edit/profile-edit.component';
import { ProfileHomeComponent } from './profile/components/profile-home/profile-home.component';

import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProfilePhotoUploadComponent } from './profile/components/profile-photo-upload/profile-photo-upload.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NavbarProfileComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    RegVerifyComponent,
    ChangePasswordComponent,
    PartnerProfileComponent,
    ProfileEditComponent,
    ProfileHomeComponent,
    ProfilePhotoUploadComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,AngularDateTimePickerModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    AppRoutingModule
  ],
  providers: [ CommonService, AuthService, AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService, 
    multi: true
  } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
