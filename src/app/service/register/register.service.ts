import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Urls } from '../../shared/urls'

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private _userAvailableUrl = Urls.USER_AVAILABLE_URL

  constructor(private http: HttpClient) { }

  userAvailable(user) {
    return this.http.post<any>(this._userAvailableUrl, user)
  }

}
