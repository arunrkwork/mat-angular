import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Urls } from '../../shared/urls'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _loginUrl = Urls.LOGIN_URL
  private _forgotPassUrl = Urls.FORGOT_PASS_URL
  private _changePassUrl = Urls.CHANGE_PASS_URL

  constructor(private http: HttpClient, 
    private _router: Router) { }

  userLogin(user) {
    return this.http.post<any>(this._loginUrl, user)
  }

  forgotPassword(user) {
    return this.http.post<any>(this._forgotPassUrl, user)
  }

  changePassword(user) {
    return this.http.post<any>(this._changePassUrl, user)
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }

  logoutUser() {
    localStorage.removeItem('token')
    this._router.navigate(['/login'])
  }
 
}
