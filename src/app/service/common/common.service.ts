import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Urls } from '../../shared/urls'
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CommonService {

  private _userAvailableUrl = Urls.USER_AVAILABLE_URL
  private _regProfUrl = Urls.REG_PROF_URL

  private _bodyType = Urls.BODY_TYPE_URL
  private _profileCreator = Urls.PROF_CRE_URL
  private _religion = Urls.RELIGION_URL
  private _caste = Urls.CASTE_URL
  private _maritalStatus = Urls.MARITAL_STATUS_URL
  private _physicalStatus = Urls.PHY_STATUS_URL
  private _familyStatus = Urls.FAMILY_STATUS_URL
  private _familyType = Urls.FAMILY_TYPE_URL
  private _familyValue = Urls.FAMILY_VALUE_URL
  private _education = Urls.EDUCATION_URL
  private _empType = Urls.EMP_TYPE_URL
  private _occupation = Urls.OCCUPATION_URL
  private _currencyType = Urls.CURR_TYPE_URL
  private _annIncome = Urls.ANN_INCOME_URL
  private _languages = Urls.LANGUAGES_URL

  private authorizationData = 'Basic ' + btoa('admin' + ':' + 'supersecret');
  private headerOptions = {
    headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization',
        'Content-Type':  'application/json',
        'Authorization': this.authorizationData
    })
  };

  constructor(private http: HttpClient) { }

  getData(): Observable<any> {
    const profileCreator = this.getProfileCreator()
    const maritalStatus = this.getMaritalStatus()
    const physicalStatus = this.getPhysicalStatus()
    const languages = this.getLanguages()
    const religion = this.getReligion()
    const caste = this.getCaste()
    const fv = this.getFamilyValue()
    const ft = this.getFamilyType()
    const fs = this.getFamilyStatus()
    const edu = this.getEducation()
    const empType = this.getEmployeeType()
    const occupation = this.getOccupation()
    const currType = this.getCurrencyType()
    const annIncome = this.getAnnIncome()
    return forkJoin([profileCreator, 
      maritalStatus, 
      physicalStatus, 
      languages,
      religion, 
      caste,
      fv, 
      ft,
      fs, 
      edu, 
      empType,
      occupation, 
      currType,
      annIncome]);
  }

  userAvailable(user) {
    return this.http.post<any>(this._userAvailableUrl, user)
  }

  registerProfile(user) {
    return this.http.post<any>(this._regProfUrl, user)
  }

  getBodyType() {
    return this.http.get<any>(this._bodyType)
  }

  getProfileCreator() {
    return this.http.get<any>(this._profileCreator)
  }

  getReligion() {
    return this.http.get<any>(this._religion)
  }

  getCaste() {
    return this.http.get<any>(this._caste)
  }

  getMaritalStatus() {
    return this.http.get<any>(this._maritalStatus)
  }

  getPhysicalStatus() {
    return this.http.get<any>(this._physicalStatus)
  }

  getFamilyStatus() {
    return this.http.get<any>(this._familyStatus)
  }

  getFamilyType() {
    return this.http.get<any>(this._familyType)
  }

  getFamilyValue() {
    return this.http.get<any>(this._familyValue)
  }

  getEducation() {
    return this.http.get<any>(this._education)
  }

  getEmployeeType() {
    return this.http.get<any>(this._empType)
  }

  getOccupation() {
    return this.http.get<any>(this._occupation)
  }

  getCurrencyType() {
    return this.http.get<any>(this._currencyType)
  }

  getAnnIncome() {
    return this.http.get<any>(this._annIncome)
  }

  getLanguages() {
    return this.http.get<any>(this._languages)
  }

  get windowRef() {
    return window
  }

}
