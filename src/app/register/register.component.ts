import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common/common.service';
import { Utils } from '../shared/utils' 
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
 
  date : Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'yyyy-MM-dd',
    defaultOpen: false,
    closeOnSelect: true
  };

  bodyTypeArray = []
  profileCreatorArray = []
  religionArray = []
  casteArray = []
  maritalStatusArray = []
  physicalStatusArray = []
  fsArray = []
  ftArray = []
  fvArray = []
  eduArray = []
  empTypeArray = []
  occupationArray = []
  currTypeArray = []
  annIncomeArray = []
  languageArray = []

  heightArray = []
  profileCreatorId = null
  profileName = ''
  mobileNumber = ''
  
  error: boolean = false
  progress:  boolean = false
  message: string = ''

 
  isLoadingResults = true;
  regForm = new FormGroup({
    proCreId: new FormControl('', Validators.required),
    pName: new FormControl('',  [
      Validators.required,
      Validators.minLength(4)    
    ]),
    pDob: new FormControl('', Validators.required),
    pGender: new FormControl('', Validators.required),
    pHeight: new FormControl('', Validators.required),
    pMobNo1: new FormControl('', Validators.required),
    pEmail: new FormControl('', Validators.required),
    pPassword: new FormControl('', Validators.required),
    pMaritalStatusId: new FormControl('', Validators.required),
    pPhysicalStatusId: new FormControl('', Validators.required),
    pMotherTongueId: new FormControl('', Validators.required),
    riReligionId: new FormControl('', Validators.required),
    riCasteId: new FormControl('', Validators.required),
    riSubCaste: new FormControl(''),
    fdFvId: new FormControl('', Validators.required),
    fdFtId: new FormControl('', Validators.required),
    fdFsId: new FormControl('', Validators.required),
    piHighEduId: new FormControl('', Validators.required),
    piEmpTypeId: new FormControl('', Validators.required),
    piOccTypeId: new FormControl('', Validators.required),
    piAnnIncomeCurrId: new FormControl('', Validators.required),
    piAnnIncomeId: new FormControl('', Validators.required),
  });

  constructor(private _commonService: CommonService,
     private _router: Router,
     private fb: FormBuilder) { }

  ngOnInit() {  

    // this.regForm = this.fb.group({
    //   pName: ['', Validators.required]
    // })

    this.initValues()
    this.generateHeight()
    this.getData()
  }

  initValues() {
    let userJson = JSON.parse(localStorage.getItem('userData'))
    
    this.profileCreatorId = userJson.proCreId
    this.profileName = userJson.pName
    this.mobileNumber = userJson.pMobNo1

    this.regForm.patchValue({
      pName: this.profileName,
      pMobNo1: this.mobileNumber
    })
  }

  generateHeight() {
    this.heightArray = Utils.heightArray
  }

  getData() {
    this._commonService.getData() 
      .subscribe(res => { 
        this.profileCreatorArray = res[0];
        this.maritalStatusArray = res[1];
        this.physicalStatusArray = res[2];
        this.languageArray = res[3];
        this.religionArray = res[4];
        this.casteArray = res[5]
        this.fvArray = res[6];
        this.ftArray = res[7];
        this.fsArray = res[8];
        this.eduArray = res[9];
        this.empTypeArray = res[10];
        this.occupationArray = res[11];
        this.currTypeArray = res[12];
        this.annIncomeArray = res[13];
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  register() {

    this.progress = true;
    let values = this.regForm.value  
    this._commonService.registerProfile(values)
      .subscribe(
        res => { 
          console.log(res)
          this.progress = false;
          this.error = res.error
          this.message = res.message
          if(!this.error) { 
            localStorage.setItem('profileId', res.id)
            //localStorage.setItem('temp-token', res.sessionToken)

            //this._router.navigate(['/verify-mobile-number'])
            localStorage.setItem('token', res.sessionToken)
            this._router.navigate(['/profile-home'])//profile-home
            console.log("success reg")
          } else console.log("reg failed")
        },
        err => { 
          this.progress = true;
          this.message = 'Connection error'
          console.log(err)
        }
      )
    
  }

}
