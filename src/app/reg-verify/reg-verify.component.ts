import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common/common.service';
import * as firebase from 'firebase'; 
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
 
@Component({
  selector: 'app-reg-verify',
  templateUrl: './reg-verify.component.html',
  styleUrls: ['./reg-verify.component.scss']
})
export class RegVerifyComponent implements OnInit {

  windowRef: any;
 

  verificationCode: string;

  user: any;
  _pMobNo1: string = null
 
  
  constructor(private _commonService: CommonService) { }

  ngOnInit() {

    this._pMobNo1 = JSON.parse(localStorage.getItem('userData')).pMobNo1
    console.log(this._pMobNo1)

    var firebaseConfig = {
      apiKey: "AIzaSyDXOcvpTtaUe7GlgPFZTYUASblsVDHP2nU",
      authDomain: "matrimonial-8a296.firebaseapp.com",
      databaseURL: "https://matrimonial-8a296.firebaseio.com",
      projectId: "matrimonial-8a296",
      storageBucket: "",
      messagingSenderId: "756716653454",
      appId: "1:756716653454:web:45410c86a7a7ec544b72df"
    };

    firebase.initializeApp(firebaseConfig);
    this.windowRef = this._commonService.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')

    this.windowRef.recaptchaVerifier.render()
  }
 

  sendLoginCode() {

    const appVerifier = this.windowRef.recaptchaVerifier;

    const num = '+91' + this._pMobNo1;

    firebase.auth().signInWithPhoneNumber(num, appVerifier)
            .then(result => { this.windowRef.confirmationResult = result; })
            .catch( error => console.log(error) );

  }

  verifyLoginCode() {
    this.windowRef.confirmationResult
                  .confirm(this.verificationCode)
                  .then( result => {

                    this.user = result.user;

    })
    .catch( error => console.log(error, "Incorrect code entered?"));
  }


}
