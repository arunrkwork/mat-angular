import { Component, OnInit } from '@angular/core';
import { Profile } from '../../../model/Profile'

@Component({
  selector: 'app-profile-home',
  templateUrl: './profile-home.component.html',
  styleUrls: ['./profile-home.component.scss']
})
export class ProfileHomeComponent implements OnInit {
  isDisabled = true
  private profile: Profile
  private pId = null
  
  constructor() {
    this.profile = new Profile()
   }

  ngOnInit() {
    this.pId = this.profile.getCurrenctUserId()
    console.log("currect user id : " + this.pId)
  }

  

  callEnable(){
    this.isDisabled = false; 
  }

}
