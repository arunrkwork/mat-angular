import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../service/http.service';
import { ActivatedRoute } from '@angular/router';
import { Profile } from '../../../model/Profile'

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  private profile: Profile;

  constructor(private httpService: HttpService, 
    private _activeRouter: ActivatedRoute) {
      this.profile = new Profile()
     }

  ngOnInit() {
    let pId = this._activeRouter.snapshot.params.id
    this.httpService.getProfile(pId)
      .subscribe(
        res => { 
          this.profile.pId = res[0].p_id
          this.profile.pMid = res[0].p_mid
          this.profile.name = res[0].p_name
          this.profile.pDob = res[0].p_dob
          this.profile.pHeight = res[0].p_height
          console.log(this.profile)
        },
        err => console.log(err)
      )
  }

}
