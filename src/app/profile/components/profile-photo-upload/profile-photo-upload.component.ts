import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../../service/http.service';
import { Profile } from '../../../model/Profile'
import { Urls } from '../../../shared/urls'

declare var $;
@Component({
  selector: 'app-profile-photo-upload',
  templateUrl: './profile-photo-upload.component.html',
  styleUrls: ['./profile-photo-upload.component.scss']
})
export class ProfilePhotoUploadComponent implements OnInit {

  private profile: Profile
  private pId = null

  selectedFile: File;
  fileList: File[] = [];
  listOfFiles: any[] = [];
  existingProfilePhotos = []

  pfUploadMsg  = "";
  pfPic = false;
  profileBaseUrl = ''
  result
  
  @ViewChild('attachments',{static:false}) attachment: any;

  constructor(private http: HttpService) {
    this.profile = new Profile()
   }

  ngOnInit() { 
    this.pId = this.profile.getCurrenctUserId()
    this.http.getProfilePhotos(this.pId)
    .subscribe(
      res => { 
        this.result = res
        if(!this.result.error) {
          this.profileBaseUrl = this.result.path 
          this.existingProfilePhotos = this.result.data
        } else this.existingProfilePhotos = null
      },
      err => { 
        this.existingProfilePhotos = null
        console.log(err)
      }
    ) 
  }
  
  onFileChanged(event: any) {
        var selectedFile = event.target.files[0];
        this.fileList.push(selectedFile);
      var count = $(".pf-preview-dv").find('img').length;
      // console.log("count"+count);
      this.pfUploadMsg  = "";
      this.pfPic = true;	

      if(count <= 3){
        var reader = new FileReader();
          reader.onload = (event:any) => {
              this.listOfFiles.push(event.target.result); 
          }
      
          reader.readAsDataURL(selectedFile);
          this.pfUploadMsg  = "";
          this.pfPic = true;
      }else{
        this.pfUploadMsg = "You can upload four Images only";
        this.pfPic = true;
      }
      
  }
 
  
  removeSelectedFile(index) {
   // Delete the item from fileNames list
   this.listOfFiles.splice(index, 1);
   // delete file from FileList
   this.fileList.splice(index, 1);
   this.pfUploadMsg  = "";
   this.pfPic = true;
   var count = $(".pf-preview-dv").find('img').length;
   if(count <= 1) {
    this.pfPic = false;
   }
  }

  uploadProfilePhotos() {
    let formData = new FormData();
    // for(let i =0; i < this.fileList.length; i++) { 
    //   formData.append("file", this.fileList[i]);
    // } 
    
    for(let img of this.fileList){
      formData.append('file', img);
    }
    this.http.uploadProfilePhotos(this.pId,formData)
    .subscribe(
      res => {
        if(res.err) {
          this.pfUploadMsg = "File not uploaded"
        } else {
          this.pfUploadMsg = res.message
        }
      },
      err => console.log(err)
    )
  }

}