import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Urls } from '../../shared/urls'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private _userUrl = Urls.USER_URL
  private _profileUploadUrl = Urls.PROFILE_UPLOAD_URL

  constructor(private http: HttpClient) { }

  getProfile(id) {
    return this.http.get<any>(this._userUrl + id)
  }

  uploadProfilePhotos(id, formData) { 
    return this.http.post<any>(this._profileUploadUrl + id, formData)
  }

  getProfilePhotos(id) {
    return this.http.get(this._profileUploadUrl + id)
  }

}
