import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../service/common/common.service'; 
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  profileCreatorArray = []
  castesArray = []
  userData = {}
  userAvailableData = {
    userName: null
  }
  private _mobileNumber: string
  private message = null

  regForm = new FormGroup({
    proCreId: new FormControl('', Validators.required),
    pName: new FormControl('',  [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(25)    
    ]),
    pMobNo1: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.pattern('[0-9]+') ])
  });

  constructor(private _commonService: CommonService, 
    private _router: Router) { }

  ngOnInit() {
    this.loadDropDowns()   
  }

  loadDropDowns() {
    if(!localStorage.getItem('profileCreator')) {  
      this._commonService.getProfileCreator()
      .subscribe( 
        res => {  
         // console.log(res);
          localStorage.setItem('profileCreator', JSON.stringify(res))                  
          this.profileCreatorArray = JSON.parse(localStorage.getItem('profileCreator'))                
        },
        err => console.log(err)
      ) 

    } else { 
        this.profileCreatorArray = JSON.parse(localStorage.getItem('profileCreator'))      
    }   
    
    if(!localStorage.getItem('caste')) { 
      this._commonService.getCaste()
      .subscribe(
        res => {  
       //   console.log(res);
          localStorage.setItem('caste', JSON.stringify(res))               
          this.castesArray = JSON.parse(localStorage.getItem('caste'))                
        },
        err => console.log(err)
      )    
    } else { 
        this.castesArray = JSON.parse(localStorage.getItem('caste'))    
    }
  }
 
  userLogin(){
    this._router.navigate(['login']);
  }
  
  userRegister(){     
    let values = this.regForm.value  
    this.userAvailableData.userName = values.pMobNo1
    this._commonService.userAvailable(this.userAvailableData)
      .subscribe(        
        res => {
          console.log(res);
          if(res.isAvailable == 1) {
              this.message = "Mobile already exists"
          } else if(res.isAvailable == 2) {
            this.message = "ok"
            localStorage.setItem('userData', JSON.stringify(values))
            this._router.navigate(['register']);
          } else {
            this.message = "error"
          }
        },
        err => console.log(err)
      ) 
  }

  userAvailable() {
    
  }

  // check(values) {
    //(ngModelChange)="check($event)" add in template
  //   console.log(values)

  //   if(values.length != 10) return

  //   this.userAvailableData.userName = values
  //   this._commonService.userAvailable(this.userAvailableData)
  //     .subscribe(        
  //       res => {
  //         console.log(res);
  //         if(res.isAvailable == 1) {
  //             this.message = "Mobile already exists"
  //         } else if(res.isAvailable == 2) {
  //           this.message = "ok"
  //         //  localStorage.setItem('userData', JSON.stringify(values))
  //          // this._router.navigate(['register']);
  //         } else {
  //           this.message = "error"
  //         }
  //       },
  //       err => console.log(err)
  //     ) 
  // }

}
