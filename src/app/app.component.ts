import { Component } from '@angular/core';
import { AuthService } from './service/auth/auth.service';
import { Router } from '@angular/router';
declare var $;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { 

  title = 'kvMatrimonyWeb';

  constructor(private _authService: AuthService) {

  }

  ngOnInit(){
  $(function() {
    $('.carousel').carousel({
      interval: 1900,
      cycle: true,
      pause: "false"
    });
  });
 

  }
 

}
