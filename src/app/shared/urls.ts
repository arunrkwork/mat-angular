export class Urls {
    
    public static BASE_URL = 'http://192.168.1.100:5001'

    public static LOGIN_URL = Urls.BASE_URL + '/users/login/'
    public static REG_PROF_URL = Urls.BASE_URL + '/users/reg/'
    public static FORGOT_PASS_URL = Urls.BASE_URL + '/users/forgotPass/'
    public static CHANGE_PASS_URL = Urls.BASE_URL + '/users/changePass/'
    public static USER_AVAILABLE_URL = Urls.BASE_URL + '/users/available/'
    
    public static BODY_TYPE_URL = Urls.BASE_URL + '/bodyType'
    public static PROF_CRE_URL = Urls.BASE_URL + '/profileCreator'
    public static RELIGION_URL = Urls.BASE_URL + '/religion' 
    public static CASTE_URL = Urls.BASE_URL + '/caste'
    public static MARITAL_STATUS_URL = Urls.BASE_URL + '/maritalStatus'    
    public static PHY_STATUS_URL = Urls.BASE_URL + '/physicalStatus'
    public static FAMILY_STATUS_URL = Urls.BASE_URL + '/familyStatus'
    public static FAMILY_TYPE_URL = Urls.BASE_URL + '/familyType'
    public static FAMILY_VALUE_URL = Urls.BASE_URL + '/familyValue'
    public static EDUCATION_URL = Urls.BASE_URL + '/education'
    public static EMP_TYPE_URL = Urls.BASE_URL + '/empType'
    public static OCCUPATION_URL = Urls.BASE_URL + '/occupation'
    public static CURR_TYPE_URL = Urls.BASE_URL + '/currencyType'
    public static ANN_INCOME_URL = Urls.BASE_URL + '/annIncome' 
    public static LANGUAGES_URL = Urls.BASE_URL + '/languages' 
     
    public static USER_URL = Urls.BASE_URL + '/users/'
    public static PROFILE_UPLOAD_URL = Urls.USER_URL + 'upload/' 
}