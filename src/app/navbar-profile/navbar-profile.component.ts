import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth/auth.service';
declare var $;
@Component({
  selector: 'app-navbar-profile',
  templateUrl: './navbar-profile.component.html',
  styleUrls: ['./navbar-profile.component.scss']
})
export class NavbarProfileComponent implements OnInit {

  constructor(private _authService: AuthService) { }

  ngOnInit() { 
  $(function() {
    $('.carousel').carousel({
      wrap: false  
    });
  });
  }

}
