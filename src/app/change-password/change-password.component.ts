import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/auth/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  progress: boolean = false
  message: string = null

  changePassData = {
    profileId : null
  }


  profileId = null;
  constructor(private _authService: AuthService, 
        private _activeRouter: ActivatedRoute,
        private _router: Router) { }

  ngOnInit() {
    
    this.progress = false
    this.message = null 

    this._activeRouter.paramMap.subscribe(params => { 
      if(this._activeRouter.snapshot.params.id)
        this.profileId = this._activeRouter.snapshot.params.id
        else 
        this.profileId = null
    });
  }

  changePassword() {
    this.progress = true
    this.changePassData.profileId = this.profileId 
    this._authService.changePassword(this.changePassData)
      .subscribe(
        res => { 
          this.progress = false          
          if(res.error) {
            this.message = res.message
          } else {
            this.message = res.message
            this._router.navigate(['/login'])
          }
        },
        err => { 
          this.progress = false
          console.log(err) 
        }
      )
  }

}
