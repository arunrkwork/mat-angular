export class Profile {

    public pId;
    public pMid
    public pProCreId
    public name
    public pDob
    public pGender
    public pHeight
    public pWeight
    public pMobNo1
    public pMobNo2
    public pEmail
    public pPassword
    birthdate: Date

    public getCurrenctUserId() {
        return localStorage.getItem('profileId')
    }

    public setPId(pId) {
        this.pId = pId
    }

    public getPid() {
        return this.pId;
    }

    public setPMid(pMid) {
        this.pMid = pMid
    }

    public getPMid() {
        return this.pMid;
    }
 

    public setPname(name) {
        this.name = name
    }

    public getPname() {
        return this.name;
    }

    public getAge(dob) {
        
        this.birthdate = new Date()
        this.birthdate = dob
        let timeDiff = Math.abs(Date.now() - this.birthdate.getTime());
        let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
        console.log(age)
        return age
    }
 

}