import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegVerifyComponent } from './reg-verify/reg-verify.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthGuard } from './service/auth/auth.guard';
import { ProfileHomeComponent } from './profile/components/profile-home/profile-home.component';
import { ProfileEditComponent } from './profile/components/profile-edit/profile-edit.component';
import { PartnerProfileComponent } from './profile/components/partner-profile/partner-profile.component';
import { ProfilePhotoUploadComponent } from './profile/components/profile-photo-upload/profile-photo-upload.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path : 'profile-home', component: ProfileHomeComponent, canActivate: [AuthGuard] },
  { path : 'profile-edit/:id', component: ProfileEditComponent, canActivate: [AuthGuard] },
  { path: 'partner-profile', component: PartnerProfileComponent, canActivate: [AuthGuard] },
  { path: 'verify-mobile-number', component: RegVerifyComponent },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'change-password/:id', component: ChangePasswordComponent },
  { path: 'profile-upload', component: ProfilePhotoUploadComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
