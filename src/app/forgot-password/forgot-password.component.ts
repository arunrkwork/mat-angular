import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../service/auth/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  progress: boolean = false
  message: string = null
  forgotPassData = {}

  constructor(private _authService: AuthService, 
        private _router: ActivatedRoute) { }

  ngOnInit() {
    this.progress = false
    this.message = null 
  }

  forgotPassword() {
    
    this.progress = true;
    this._authService.forgotPassword(this.forgotPassData)
      .subscribe(
        res => { 
          this.progress = false;
          if(res.error) this.message =  res.message
          else this.message = "Please check your mail"
        },
        err => {
          this.progress = false;
          console.log(err)
        }
      )
  }


}
