import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router'; 
import { AuthService } from '../service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  
  progress:  boolean = false
  message: string = null

  loginUserData = {}
  @ViewChild('userNameRef', { static: false }) userNameElementRef: ElementRef
  
  constructor(private _authService: AuthService,
      private _router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.userNameElementRef.nativeElement.focus()
  }

  userLogin() {
    this.progress = true;
    console.log(this.loginUserData)
    this._authService.userLogin(this.loginUserData)
      .subscribe(
        res => { 
          if(res.error == false) {
            this.progress = false;
            localStorage.setItem('profileId', res.id)
            localStorage.setItem('token', res.sessionToken)
            this._router.navigate(['/profile-home'])
          } else {
            this.progress = false; 
            this.message = res.message
          }          
        },
        err => { 
          this.progress = false;
          this.message = err
          console.log(err)
        }
      )
  }

}
