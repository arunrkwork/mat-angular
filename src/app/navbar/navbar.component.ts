import { Component, OnInit } from '@angular/core';
declare var $;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      $(function() {
        window.addEventListener("hashchange", function () {
          window.scrollTo(window.scrollX, window.scrollY - 100);
      });
    });
  }

}
